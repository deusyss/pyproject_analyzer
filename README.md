# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is pyproject-analyzer? ###

Pyproject-analyzer is the first tool of several ones, and first step of a global process, which will allows to create audit suite, using communauty and standard tools:

* pylint
* radon
* McCabe
* Pydocstyle
* ...

### How does it works? ###

As an audit tool, its goal is not to integrate code into another audit one to inspect it with dedicated lib as inspect for example.

Instead of this approach, this tool is a static analyze based on REGEX to detect:
* Package
* Package, with PEP420 respect
* Python Module (with .py extension only for the moment)
* Class definition
* Method definition
* Function definition
* import names


### What have we get in output ###

In output, you'll get a dictionnary which contains:
* A statistic part
* The full path to package
* The full path to module
* For each package, the module and subpackage list
* For each module, the class and function included
* For each class, the methods included 

### Licence ###

* MIT

### Contribution guidelines ###

* Writing tests
* Code review
* Documentation
* ...

### Initial Authors ###

* GALODE Alexandre

### Contributors ###

* MOULLEC Kevin
* DIVET Benjamin
