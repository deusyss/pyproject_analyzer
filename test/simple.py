import os
import sys
from math import sqrt


def function01():
    pass

def function02():
    import time
    import os

class Class01():

    def __init__(self):
        pass

def function03():
    pass

def function04():
    pass

class Class02():

    def __init__(self):
        import winreg
        from math import sinh

class Class03():

    def __init__(self):
        pass

def function05():
    from math import sqrt
    from winreg import error

def function06():
    pass

if __name__ == "__main__":
    from os import path