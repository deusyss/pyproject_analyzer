import json

class JsonFormater(object):
    def __init__(self):
        pass

    def format_dict(self, pj_dict):
        """
        Allow to convert the given dictionary to JSON format

        :param pj_dict: the dictionary which contains the analyze of the project
        :type pj_dict: dict
        :return: the content of the porject dictionary translate in JSON format
        :rtype: json
        """

        json_output = json.dumps(pj_dict, sort_keys=True, indent=4, ensure_ascii=False)

        return json_output