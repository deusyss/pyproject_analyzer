from setuptools import setup
import os

import pyproject_analyzer


setup(name = 'pyproject_analyzer',
      version = pyproject_analyzer.__version__,
      platforms = 'ALL',
      url = 'http://mon_projet.org',
      download_url = 'http://packages.python.org/mon_projet',
      description = 'First module of PYCAudit (Python Code Audit) which aims to purpose an complet audit tool for Python Code',
      long_description = open(os.path.join(os.path.dirname(__file__), 'SETUP_README')).read(),
      keywords = 'project analyze audit setup.py',
      packages = ['pyproject_analyzer'],
      classifiers = ["Development Status :: 1 - Planning",
                     "Environment :: Console",
                     "Topic :: Software Development :: Documentation",
                     "Topic :: Software Development :: Quality Assurance",
                     "License :: OSI Approved :: MIT License"]
     )